import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MailActionComponent } from './mail-action.component';

describe('MailActionComponent', () => {
  let component: MailActionComponent;
  let fixture: ComponentFixture<MailActionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MailActionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MailActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
