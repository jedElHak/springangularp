import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TestimontialComponent } from './testimontial.component';

describe('TestimontialComponent', () => {
  let component: TestimontialComponent;
  let fixture: ComponentFixture<TestimontialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TestimontialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestimontialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
