import { Component,Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators,FormsModule, NgForm } from '@angular/forms';
import { AuthentificationService } from '../authentification.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgModule } from '@angular/core';
import { Injectable } from "@angular/core";





@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@Injectable()
export class LoginComponent implements OnInit {

  @Input()loginForm: FormGroup;
  @Input()returnUrl: string;
  @Input() static submitted=false;
  pageName:String;

  constructor(private fb: FormBuilder,
    private auth: AuthentificationService,
    private route: ActivatedRoute,
    private router: Router,
  
  ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      'email' : [null, [Validators.required, Validators.email]],
      'password' : [null, Validators.required],
    });

    this.returnUrl = this.route.snapshot.queryParams['/profile'] ;

  }

  opened=false;
  toggleSidebar(){
    this.opened=!this.opened;
  }

 /* login(formData: NgForm) {
  
    return this.auth.login(formData).subscribe(
      (user) => {
        console.log(user);
        this.router.navigate(['/profile']);
      });
  }*/
  goTopage(){
    this.router.navigate([this.pageName]);
  }
@Input() hide() {
   LoginComponent.submitted=!LoginComponent.submitted;
   

}

}