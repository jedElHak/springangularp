import { NgModule } from '@angular/core';

import {
    MatToolbarModule,} from '@angular/material/toolbar';
    import {MatIconModule} from '@angular/material/icon';
    import {MatSidenavModule} from '@angular/material/sidenav';
    import {MatListModule} from '@angular/material/list';
    import {MatButtonModule} from '@angular/material/button';
    import {MatBadgeModule} from '@angular/material/badge';
    import {MatCardModule} from '@angular/material/card';







@NgModule({
    
    imports: [
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatBadgeModule,
        MatCardModule
    ],
    exports: [
        MatToolbarModule,
        MatIconModule,
        MatSidenavModule,
        MatListModule,
        MatButtonModule,
        MatBadgeModule,
        MatCardModule
    ]    
  })
  export class MaterialModule { }