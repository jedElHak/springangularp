import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import {  ActivatedRoute,Router } from '@angular/router';
import { AuthentificationService } from '../authentification.service';
import { Product } from '../Product';
import { User } from '../user';



@Component({
  selector: 'app-post-product',
  templateUrl: './post-product.component.html',
  styleUrls: ['./post-product.component.css']
})
export class PostProductComponent implements OnInit {
  postForm :FormGroup;
  opened :boolean=false;
  img;
  message="";
  imageAct:boolean;
  prod=new Product();
  prods:Product[];
  user= new User();
  receivedImageData:any;
 base64Data;
 convertedImage:any;
 

  constructor(private fb :FormBuilder,private auth:AuthentificationService,private route :ActivatedRoute) { }

  ngOnInit() {
    this.postForm=this.fb.group({
      'name':[null,Validators.required],
      'image':[null,Validators.required],
      'price':[null,Validators.required],
      'description':[null,Validators.required]


    });
   // this.getImage();

    this.toggle();
    this.saveProduct();
    this.prods=[];
    this.prod;
  }
  FileSelected(event){
    console.log(event);
    this.prod.image=event.target.files[0];
    console.log("prod.image="+this.prod.image);

  }
  //save product in backend 
 saveProduct(){
   const id=parseInt(this.route.snapshot.paramMap.get('id'));

    this.prod.name=this.postForm.get("name").value;
    this.prod.price=this.postForm.get("price").value;
    this.prod.description=this.postForm.get("description").value;
    const UploadImageData=new FormData();
   // UploadImageData.append('id',JSON.stringify(this.prod.id));
   UploadImageData.append('id',JSON.stringify(id))
    UploadImageData.append('name',JSON.stringify(this.prod.name));
    UploadImageData.append('image',this.selectedFIle);
    UploadImageData.append('price',JSON.stringify(this.prod.price));
    UploadImageData.append('description',JSON.stringify(this.prod.description));

  

    this.auth.setProducts( UploadImageData).subscribe(
      res=>this.prods=res,
    );
      this.opened=false;

  }
  selectedFIle:File=null;
  onFileSelected(event){
    console.log(event);
    this.selectedFIle=event.target.files[0];
    console.log( this.selectedFIle);

   
    

  }
  
  //get boolean value from profile.ts by subscibtion to boolean subject :show the post form
  toggle(){
  
    this.auth.booleanimport.subscribe({
     
     next: value=>this.opened=value
     
    });
    console.log("prod"+this.opened)
  }
/*getImage() {
    //Make a call to Spring Boot to get the Images Bytes.
    this.user.id=parseInt( this.route.snapshot.paramMap.get('id'));
    const UploadImageData=new FormData();
    UploadImageData.append('id',JSON.stringify(this.user.id));
    console.log(this.user.id);
    this.auth.getProductImage(UploadImageData).subscribe(
        res => {console.log("ici ces les images"+res);
          this.receivedImageData=res;
          this.base64Data=this.receivedImageData.logo;
          this.convertedImage='data:image/jpeg;base64,'+this.base64Data;
          localStorage.setItem("receivedImageProduct",JSON.stringify(this.receivedImageData));
          } );
          
        }*/
}
