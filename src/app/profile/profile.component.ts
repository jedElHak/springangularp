import { Component, Input, OnInit, Output } from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { FormGroup, FormBuilder, Validators, NgForm ,AbstractControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Profile } from '../Profile';
import { profile } from 'console';
import * as Long from 'long';
import { Observable, Subscriber, timer } from 'rxjs';
import { timeout } from 'rxjs/operators';
import { Product } from '../Product';
import { User } from '../user';
import { isInteger } from '@ng-bootstrap/ng-bootstrap/util/util';






@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  loginForm: FormGroup;
  public  profileId;
  profiles:Profile[];
  profile=new Profile();
  msg="";
  product:Product[];
  public values ;
  public products;
  user=new User();
  imgUrl:null ;
  postImage:any ;
  message:String;
receivedImageData:any;
 base64Data;
 convertedImage:any;
 imageAct:boolean;
 postBoolean=false;
 images:Product[];
 

  subscriber:Observable<Profile[]>;
 

  constructor(private auth:AuthentificationService,private http: HttpClient,private fb: FormBuilder,private route:ActivatedRoute,private router:Router) { }

  ngOnInit(): void {
  //  this.getProduct();
    this.loginForm = this.fb.group({
      'email' : [null],
      'password' : [null],
      'repeatpassword' : [null],
    });
    this.profile;
    this.profiles=[];
    this.product=[];
    this.user.id;
    this.images=[];
    
    this.receivedImageData1;
    this.getImage();
    this.getImageProduct()
    //this.getProduct();
    this.receivedImageData;
  
    this.values =JSON.parse(localStorage.getItem('user'));
    this.images =JSON.parse(localStorage.getItem('receivedImageProduct'));
    this.products =JSON.parse(localStorage.getItem('product'));
    this.imageProduct();
   
  
  
  this.route.paramMap.subscribe(
     
    ()=>{
       
       this.onUpload();
     }
   );
  this.route.paramMap.subscribe(
     
   ()=>{
       
       this.getImage();
     }
   );
   this.post();
  }
  
  opened=false;
  selectedFIle=null;


  @Output() toggle(){
  
  this.auth.booleanimport.subscribe({
   next: value=>this.opened=value
   
  });
  
  return !this.opened
     }
     onFileSelected(event){
       console.log(event);
       this.selectedFIle=event.target.files[0];
       console.log(this.selectedFIle);

       var reader=new FileReader();
       reader.onload=(event:any)=>{
         this.imgUrl=event.target.result;
       }
       reader.readAsDataURL(this.selectedFIle);
       this.imageAct=!this.imageAct;
       

     }
     onUpload(){
       this.user.id=parseInt(this.route.snapshot.paramMap.get('id'));
       const UploadImageData=new FormData();
       this.user.logo=this.selectedFIle ;
       UploadImageData.append('logo',this.selectedFIle,this.selectedFIle.name);
       UploadImageData.append('id',JSON.stringify(this.user.id));
     

       
       this.auth.onUpload(UploadImageData).subscribe(
         res=>{
          if (res.status === 200) {
            this.message = 'Image uploaded successfully';
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
        );



     }
     
    getProduct(){
     
      
      this.auth.getProducts().subscribe( 
         response=>this.product=response );
         localStorage.setItem('product',JSON.stringify(this.product))
        
        console.log(this.product);    
      
}
getImage() {
  //Make a call to Sprinf Boot to get the Image Bytes.
  this.user.id=parseInt( this.route.snapshot.paramMap.get('id'));
  const UploadImageData=new FormData();
  UploadImageData.append('id',JSON.stringify(this.user.id));
  console.log(this.user.id);
  this.auth.getImage(UploadImageData).subscribe(
      res => {console.log("getimage picture"+res);
        this.receivedImageData=res;
        this.base64Data=this.receivedImageData.logo;
        this.convertedImage='data:image/jpeg;base64,'+this.base64Data;
        localStorage.setItem("receivedImage",JSON.stringify(this.convertedImage));
        } );
        
}
post(){
   this.auth.toggleSidebar(this.postBoolean);
}
imageProduct(){
 
 for(let i=0;i<this.products.length;i++){
  this.products[i].image='data:image/jpeg;base64,'+this.images[i].image;
}
}
receivedImageData1:any;
 base64Data1;
 convertedImage1:any;
getImageProduct() {
  //Make a call to Spring Boot to get the Images Bytes.
  this.user.id=parseInt( this.route.snapshot.paramMap.get('id'));
  const UploadImageData=new FormData();
  UploadImageData.append('id',JSON.stringify(this.user.id));
  console.log(this.user.id);
  this.auth.getProductImage(UploadImageData).subscribe(
      res => {console.log("ici ces les images"+res);
        this.receivedImageData1=res;
        this.base64Data1=this.receivedImageData1.logo;
        this.convertedImage1='data:image/jpeg;base64,'+this.base64Data1;
        localStorage.setItem("receivedImageProduct",JSON.stringify(this.receivedImageData1));
        } );
        
      }

}
