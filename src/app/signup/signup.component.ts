import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { AuthentificationService } from '../authentification.service';
import { Router } from '@angular/router';
import { User } from '../user';
import { ReactiveFormsModule } from '@angular/forms';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  user =new User();
  signupForm: FormGroup;

  constructor(private fb: FormBuilder,
    private auth: AuthentificationService,
    private router: Router
  ) { 
    //this.auth.signup();
  }
  

  ngOnInit() {
    this.signupForm = this.fb.group({
      'firstName' : [null, Validators.required],
      'lastName' : [null, Validators.required],
      'email' : [null, [Validators.required, Validators.email]],
      'password' : [null, Validators.required],
    });
   
  }

  opened=false;
  toggleSidebar1(){
    this.opened=!this.opened;
  }

  signup(formData: NgForm) {
    return this.auth.signup(this.user).subscribe(
      (user) => {
        console.log(`added user ${JSON.stringify(user)}`);
        this.router.navigate(['user']);
      });
  }


}

