import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable , of, Subject, timer } from 'rxjs';
import { catchError, map, tap, timeout } from 'rxjs/operators';
import { LoginComponent } from './login/login.component';
import { SliderComponent } from './slider/slider.component';
import { User } from './user';
import { Profile } from './Profile';
import { Product } from './Product';





@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

 private  apiUrl = 'http://localhost:8084/api/v1/users?size=100';
 private  apiUrl2 = 'http://localhost:8084/api/v1/profiles?size=100';
 private  apiUrl3 = "http://localhost:8084/api/v1/products?size=100";
 private  apiUrl4 = 'http://localhost:8084/api/v1/upload';
 private  apiUrl5 = "http://localhost:8084/api/v1/upload1";
 private apiUrl6=" `${this.apiUrl}`+'/'+`${id}`+'/user'";





  hide:boolean;
  private booleanValue=new Subject<boolean>();
  booleanimport=this.booleanValue.asObservable();
  public id:number;
  user=new User();
  product:Product[];
  users:User[];




  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  constructor(private http: HttpClient, private route: ActivatedRoute,
    private router: Router ) { }

  signup(user:User) :Observable<User[]>{
    return this.http.post<getResponseUser>( `${this.apiUrl}`,user).pipe(
      map(response=>response._embedded.users),
      );
  }

  login(user:User):Observable<User[]> {
   const lognUrel=`${this.apiUrl}`
   return this.http.get<getResponseUser>( lognUrel).pipe(
     map(response=>response._embedded.users),
     tap(user => {
       timeout(5000)
        }

      
    ));
    
    
  }
  //get products from backend
  getProducts() :Observable<Product[]>{
    return this.http.get<getResponseProduct>( `${this.apiUrl3}`).pipe(
      
map(
  response=>response._embedded.products)
);
   
  }
  //set products to frontend 
 setProducts(dataForm:FormData) :Observable<Product[]>{
    return this.http.post<any>('http://localhost:8084/api/v1/prod/prod',dataForm) ;
   
  }

  logout() {
    if (localStorage.getItem('user')) {
      localStorage.removeItem('user');
     localStorage.removeItem('product');
      localStorage.removeItem('receivedImage');
      localStorage.removeItem('receivedImageProduct');

      

   
    }
  }
  onUpload(formdata:FormData){
    return this.http.post<any>( `${this.apiUrl4}`,formdata);
      

  }
 getImage(id:FormData){
    console.log(id);
    return this.http.post<any>( `${this.apiUrl5}`,id).pipe(
      map(res=>this.user=res),

      
    );

  }
  getProductImage(id:FormData)
 {
    console.log(id);
    return this.http.post<Product[]>('http://localhost:8084/api/v1/prod/prod1',id).pipe(
      map(res=>this.product=res),

      
    );

  }

  isloggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    } else {
      return false;
    }
  }
 
 
 
  getUserProfile(id :number):Observable<Profile[]> {
  /*  this.slidCOmponent.longimport.subscribe({
      next:value=>this.id=value
     } )*/
    const profileUrl=`${this.apiUrl2}/search/findById?id=${id}`;
    return this.http.get<getResponseProfile>( profileUrl).pipe(
      
      map( response=>response._embedded.profiles),

    );
     
 
  }
  
 
  toggleSidebar(val :boolean ) {
  val=!val;
  console.log("val="+val)
  this.booleanValue.next(val);
  return  val;
    
  }

}
interface getResponseUser{
  _embedded:{
    users:User[];
  }
  
 
}
interface getResponseProfile{

  
  _embedded:{
    profiles:Profile[];
  }
  
  
}
interface getResponseProduct{

  _embedded:{
    products:Product[];
  }
  
  
}
