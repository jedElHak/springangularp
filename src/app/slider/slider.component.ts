import { Component, OnInit,Input, Output ,EventEmitter} from '@angular/core';
import { AuthentificationService } from '../authentification.service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { Observable , of, Subject } from 'rxjs';
import { element } from 'protractor';
import { User } from '../user';
import { AngularFirestoreModule } from '@firebase/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { debounceTime,map ,take, timeout } from 'rxjs/operators';
import { idText } from 'typescript';
import { Product } from '../Product';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css'],
  providers:[]

})
export class SliderComponent implements OnInit {
  loginForm: FormGroup;
  signupForm: FormGroup;
  @Input()returnUrl: string;
  @Input() static submitted=false;
  pageName:String;
  loggedIn=false;
   users:User[];
   user=new User ();
   product:Product[];
   id:number ;
  
   msg="";

  constructor(private fb: FormBuilder,
    private auth: AuthentificationService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
    ngOnInit() {
      //create form login
      this.loginForm = this.fb.group({
        'email' : [null,Validators.required],
        'password' : [null,Validators.required]
       
      });
       //create form signup
      this.signupForm = this.fb.group({
        'email' : [null,Validators.required],
        'password' : [null,Validators.required],
        'repeatpassword' : [null,Validators.required],
        'first_name' : [null,Validators.required],
        'last_name' : [null,Validators.required]
      });
  
      this.id;
      this.users=[];
      this.getProduct();
 

      this.login();
      this.user;
      this.product=[];
      //this.signup();
      
     
      
  
    }
  opened =false;
 

 
 
  login(){
    //get DATA from BACKEND by observable object created on athentificaton service
    this.auth.login(this.user).subscribe(
     data=>this.users=data);
     
     //identify  password and login of user 
     for( let  i=0 ;i<this.users.length;i++){
      if(this.users[i].password==this.loginForm.get('password').value&&this.users[i].username==this.loginForm.get('email').value)
      {
        //if user exist  hide the backround component by using a boolean in *ngIf 
        this.opened=this.auth.toggleSidebar(this.opened);
        this.getProduct();
        this.router.navigate(['/profile',this.users[i].id]);
        //sotre data of users on memory 
        localStorage.setItem('user', JSON.stringify(this.users[i]) );
       
    
      }
      else{
        this.msg=" Invalid Email or Password";
      }
     }
    
  }
 
  signup() {
    this.user.first_name=this.signupForm.get("first_name").value;
    this.user.last_name=this.signupForm.get("last_name").value;
    this.user.username=this.signupForm.get("email").value;
    this.user.password=this.signupForm.get("password").value;
    this.opened=this.auth.toggleSidebar(this.opened);
    localStorage.setItem('user',JSON.stringify(this.user));
    this.getProduct();

    users:User
  //  console.log(this.user);
   this.auth.signup(this.user).subscribe(
      (user) =>{this.users=[];
        this.users=user
      }
        

        
      );
      console.log(this.users);
      this.router.navigate(['/profile',this.users[this.users.length-1].id+1]);
     



      
  }
  getProduct(){
     
      
    this.auth.getProducts().subscribe( 
       response=>this.product=response );
       localStorage.setItem('product',JSON.stringify(this.product))
      
      console.log(this.product);    
    
}
 @Output() public  childevent = new EventEmitter();
 receivedImageData:any;
 base64Data;
 convertedImage:any;
 message:String;


}
